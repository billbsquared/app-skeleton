const gutil = require(`gulp-util`);

function gutilLog(name, toLog) {
  gutil.log(`[${name}]`, toLog);
}

function gutilError(name, err) {
  throw new gutil.PluginError(name, err);
}

module.exports = {
  log: gutilLog,
  error: gutilError,
};
