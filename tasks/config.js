const path = require(`path`);
const webpack = require(`webpack`);

const env = process.env.NODE_ENV || `development`;
const port = process.env.PORT || 8080;

let srcDir = `src`;
let appDir = `app`;
let baseDir = path.join(__dirname, `..`);

let bundledVendor = [
  `angular`
];

let bundledRuntime = [
  `angular`,
];

let webpackConfig = {
  entry: {
    app: path.join(baseDir, srcDir),
    runtime: bundledRuntime,
    vendor: bundledVendor,
  },
  output: {
    path: path.join(baseDir, appDir),
    publicPath: `../app/`,
    filename: `[name].bundle.js`,
    chunkFilename: `[id].bundle.js`,
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      names: [`vendor`, `manifest`],
    }),
  ],
};

if (env === `development`) {
  webpackConfig.devtool = `inline-source-map`;
}

module.exports = {
  srcDir: path.join(baseDir, srcDir),
  appDir: path.join(baseDir, appDir),
  port: port,
  webpack: webpackConfig,
};
