const gulp = require(`gulp`);
const webpack = require(`webpack`);
const webpackDevServer = require(`webpack-dev-server`);

const cfg = require(`./config`);
const util = require(`./utils`);

gulp.task(`server`, [`webpack`], () => {
  let config = Object.create(cfg.webpack);

  new webpackDevServer(webpack(config), {
    stats: {
      colors: true,
    },
    hot: true,
  }).listen(cfg.port, `localhost`, err => {
    if (err) return util.error(`webpack-dev-server`, err);
    util.log(`webpack-dev-server`, `http://localhost:${port}/webpack-dev-server/app/index.html`);
  });
});
