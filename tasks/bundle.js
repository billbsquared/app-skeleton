const gulp = require(`gulp`);
const babel = require(`gulp-babel`);
const path = require(`path`);
const webpack = require(`webpack`);

const cfg = require(`./config`);
const util = require(`./utils`);

gulp.task(`babel`, () => {
  gulp.src(path.join(cfg.srcDir, `**/*.js`))
    .pipe(babel())
    .pipe(gulp.dest(cfg.appDir));
});

gulp.task(`webpack`, [], callback => {
  let config = Object.create(cfg.webpack);

  webpack(config, (err, stats) => {
    if (err) return util.error(`webpack`, err);
    util.log(`webpack`, stats.toString({
      colors: true,
      progress: true,
    }));
    callback();
  })
});
