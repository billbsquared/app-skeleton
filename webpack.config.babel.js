const path = require(`path`);
const webpack = require(`webpack`);

const env = process.env.NODE_ENV || `development`;

let config = {
  entry: {
    app: `./src/index.js`,
    vendor: [`angular`],
  },
  output: {
    path: path.join(__dirname, `public`),
    publicPath: `../public/`,
    filename: `[name].bundle.js`,
    chunkFilename: `[id].bundle.js`,
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      names: [`vendor`, `manifest`],
    }),
  ]
};

if (env === `development`) {
  config.devtool = `inline-source-map`;

}

exports = module.exports = config;
