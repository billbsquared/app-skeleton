const gulp = require(`gulp`);
const gutil = require(`gulp-util`);
const babel = require(`gulp-babel`);
const path = require(`path`);
const webpack = require(`webpack`);
const webpackConfig = require(`./webpack.config.babel`);
const webpackDevServer = require(`webpack-dev-server`);

const srcDir = path.join(__dirname, `src`);
const destDir = path.join(__dirname, `public`);
const port = process.env.PORT || 8080;

gulp.task(`default`, [`webpack`]);

gulp.task(`babel`, () => {
  gulp.src(path.join(srcDir, `**`, `*.js`))
    .pipe(babel())
    .pipe(gulp.dest(destDir));
});

gulp.task(`webpack`, [], function (callback) {
  let config = Object.create(webpackConfig);

  webpack(config, function (err, stats) {
    if (err) throw new gutil.PluginError(`webpack`, err);
    gutil.log(`[webpack]`, stats.toString({
      colors: true,
      progress: true,
    }));
    callback();
  })
});

gulp.task(`server`, [`webpack`], function () {
  let config = Object.create(webpackConfig);

  new webpackDevServer(webpack(config), {
    stats: {
      colors: true,
    },
    hot: true,
  }).listen(port, `localhost`, err => {
    if (err) throw new gutil.PluginError(`webpack-dev-server`, err);
    gutil.log(`[webpack-dev-server]`, `http://localhost:${port}/webpack-dev-server/index.html`);
  });
});
